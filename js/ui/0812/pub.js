var core = window[APP_NAME];
core.observer.on('LOAD', function(){
	btnGroupTab(); // 중복불가
	Check(); //체크박스 전체 체크&해제

	// 중복불가
	function btnGroupTab() {
		$('body').on('click', '.btn-group-tab > a, .btn-group-tab > button', function() {
			$(this).siblings().removeClass('active');
			$(this).addClass('active');
		});
	}

	//체크박스
	function Check() {
		var $allChk = $('.checkbox-wrap .all-chk input'),
			$Chk = $('.checkbox-wrap .checkbox input');

			//실행
			$allChk.on('click', $allChk , Allchk);
			$Chk.on('click', Count);

			// 전체 선택
			function Allchk() {
				if ($allChk.is(':checked')) {
					$Chk.prop('checked', true);
				} else {
					$Chk.prop('checked', false);
				}
			}
			// 갯수 체크
			function Count(){
				$isChk = $('.checkbox-wrap .checkbox').not('.all-chk').find('input').filter(':checked');
				count = $isChk.length,
				ChkLength = $Chk.length - 1;

				if(count === ChkLength){
					$allChk.prop('checked', true);
				}
				else{
					$allChk.prop('checked', false);
				}
			}
		}

	//header
	//mobile
	$('.depth1').on('touchend', function(){
		if ($(this).hasClass('active')) {
			$('.depth1').removeClass('active');
		} else {
			$('.depth1').removeClass('active');
			$(this).addClass('active');
		}
	});

	$('.mobile-menu').on('click', function(){
		$('.menu-wrap').addClass('active');
	});
	$('.mobile-menu-close').on('click', function(){
		$('.menu-wrap').removeClass('active');
	});

	//pc
	$('header nav > ul > li').on('mouseenter', function(){
		$('header nav > ul > li').addClass('opa');
		$(this).removeClass('opa');
	});
	$('header nav > ul > li').on('mouseleave', function(){
		$('header nav > ul > li').removeClass('opa');
	});

	$(window).on('scroll', function(){
		$('header .ui-select').removeClass('active');
		var scrtop = $(window).scrollTop();

		if (scrtop >= 32) {
			$('body').addClass('fixed');
		} else {
			$('body').removeClass('fixed');
		}
	});

	$('.fixed-on').on('click', function(){
		$(this).toggleClass('active');
	});

	$('.btn-bell').on('click', function(){
		$('.alarm-popup').toggleClass('active'); 
	});
	$('.alarm-popup .alarm-popup-close').on('click', function(){
		$('.alarm-popup').removeClass('active');
	});

	//footer`
	$('.family .opener').on('click', function(){
		$(this).toggleClass('active');
	});
	$('.family a').on('click', function(){
		$('.family .opener').removeClass('active');
	});
	$('footer nav button').on('click', function() {
		var parent = $(this).parent();
		if (parent.hasClass('active')) {
			parent.removeClass('active');
		} else {
			$('footer nav li').removeClass('active');
			parent.addClass('active');
		}
	});

	$('footer .info-title').on('click', function (){
		$('footer .info-wrap').toggleClass('inactive');
	});

	// add list
	$('body').on('click', '.add-list .add', function(){
		var itemLength = $('.add-list .item').length;
		if (itemLength == 5) {
			alert('사이트URL 등록개수가 초과하였습니다.\n(사이트URL은 최대 5개까지 등록가능합니다.)');
		} else {
			var copyList = $(this).closest('.item').clone();
			$(copyList).removeClass('default');
			$(copyList).find('button').prop('disabled', false);
			$(copyList).find('input').attr('value','');
			$(copyList).find('input').prop('disabled', false);
			$('.add-list').append(copyList);
		}
	});
	$('body').on('click', '.add-list .del', function(){
		$(this).closest('.item').remove();
	});

	// 셀렉트 position
	$('.ui-select.fixed button').on('click', function(){
		var offset = $(this)[0].getBoundingClientRect();
		$(this).next('ul').css({
			'position': 'fixed',
			'top':offset.top + 30,
			'left':offset.left
		});
	});
	$(window).on('scroll', function(){
		$('.ui-select.fixed').removeClass('active');
	});
	$('.list-top').on('scroll', function(){
		$('.ui-select.fixed').removeClass('active');
	});

	//체크박스 셀렉트
	$('.check-select').on('click',function(){
		$(this).toggleClass('active');
	});

	// 툴팁 모바일
	$('.ui-tooltip').on('click', function(){
		var offset = $(this)[0].getBoundingClientRect();
		$(this).children('.tooltip').css({
			'top':offset.top + 30,
		});
	});
	$(window).on('scroll', function(){
		var $el = $('.ui-tooltip').find('> label input');
		$el.prop('checked', false).trigger('change');
	});

	//담당자 추가
	$('body').on('click', '.add-manager', function(){
		var itemLength = $('.add-manager-section .h3-item-wrap').length;
		if (itemLength == 5) {
			alert('추가 담당자는 최대5개까지 등록이 가능합니다.');
		} else {
			var location = $(this).parent().prev('.add-manager-section');
			var copyList = $(location).children('.h3-item-wrap-dummy').clone();
			$(copyList).removeClass('h3-item-wrap-dummy').addClass('h3-item-wrap');
			$(location).find($('.no-data')).hide();
			$(location).append(copyList);
		}
	});
	$('body').on('click', '.add-manager-section .ask-del', function(){
		var location = $(this).parent().parent().parent().parent();
		$(this).closest('.h3-item-wrap').remove();
		var itemLength1 = $(location).children('.h3-item-wrap').length;
		if (itemLength1 == 0) {
			$(location).find('.no-data').show();
		}
	});

	// 잔액알림설정 수신여부
	//켜기
	var $trunButton   =  $( '.bank-balance-wrap .bank-bal-recept');
	$trunButton.on('click', function () {
		var $uiSelect   =  $(this).parent().parent().find('.ui-select');
		var $uiSelectTxt   =  $(this).parent().parent().find('.ui-select-txt');
		if(   $uiSelect.hasClass("disabled")   ) {
			$uiSelect.removeClass("disabled");
			$uiSelectTxt.removeClass("disabled");
			$('.checkbox').find('input').prop("disabled", false);

		} else {
			$uiSelect.addClass("disabled");
			$uiSelectTxt.addClass("disabled");
			$('.checkbox').find('input').prop("disabled", true);
		}
	});


		//MY 비즈니스 > 관리버튼 알림 클릭시
		var $tooltipBtnList =    $(".tooltip-btn-list ");
		$(".btn-list-btn,  .tooltip-btn-list ul >li>a ").on("click", function(){
			var $alertWrap  = $(this).parent(".alert-black-wrap").next();
			if( $(this).next().hasClass("active")  || $alertWrap.hasClass("active")   ) {  
				$(this).next().removeClass("active");
				$alertWrap.removeClass("active");
			} else {
				$alertWrap.addClass("active");
				$(this).next().addClass("active");
			}
		});

		
		//MY 비즈니스 >  잔액 물음표 클릭시
		var $tooltipBtns =    $(".tooltip-black ");
		$tooltipBtns.on("click", function(e){
			var $insideBtn2 = $(this).find('.tooltip'),
										x = e.pageX - 123,
			$childrenTooltip = $(this).children('.tooltip');
			if( !$insideBtn2.hasClass("active")  ) {
				if (  x < 0 ||    x < 25 ) {
					$childrenTooltip.attr("style", " left: -82px   !important ");
					$childrenTooltip.addClass("m-tooltip");
						$insideBtn2.addClass("active");
				} else {
						$insideBtn2.addClass("active");
				}
			} else {
				$insideBtn2.removeClass("active");
				$childrenTooltip.removeClass("m-tooltip");
			}
		});


		//MY 비즈니스 > 검은느낌표 마우스오버시
		$(".alert-white").hide();
			var $tooltipBtns =    $(".alert-black ");
		if ($(window).width()  < 1023 ) {
			$tooltipBtns.on("click", function () {
				$(this).parents('.sort-list').find(".alert-white").toggle();
			});
		} else {
			$tooltipBtns.hover(function () {
				$(this).parents(".sort-list").find(".alert-white").show();
			}, function () {       
				$(this).parents('.sort-list').find(".alert-white").hide();
			});
		};

		//MY 비즈니스 > 상단 sort 버튼 클릭시 이벤트
		$('.sortable').on('click',function(){
			var $btn = $(this).find('button');
			if($btn.hasClass('')){
				$btn.addClass('up')
			}else{
				$btn.toggleClass('up down');
			}
		})





})
